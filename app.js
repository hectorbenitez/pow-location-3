var express = require('express');
var bodyParser = require('body-parser');
var app = express();
var compress = require('compression');
var path = require('path');
var expressValidator = require('express-validator');
var connectAssets = require('connect-assets');

if (!process.env.PL3_BASE_URL) {
  console.log('PL3_BASE_URL not set.');
  process.exit(1);
} else if (process.env.PL3_BASE_URL.lastIndexOf('/') !== process.env.PL3_BASE_URL.length - 1) {
  process.env.PL3_BASE_URL += '/';
}

app.use(bodyParser.json());
app.set('views', path.join(__dirname, 'view'));
app.set('view engine', 'jade');
app.use(compress());
app.use(connectAssets({
  paths: [path.join(__dirname, 'public')]
}));

app.use(expressValidator());

app.use(express.static(path.join(__dirname, 'public'), { maxAge: 31557600000 }));

/**
 * Controllers (route handlers).
 */
var descriptorController = require('./controller/descriptor');
var moduleController = require('./controller/module');

/**
 * Primary app routes.
 */
['/descriptor.json', '/connect.json', '/connect'].forEach(function (route) {
  app.get(route, descriptorController.descriptor);
});

app.get('/m/:module', function(req, res) {
  moduleController.view(req.params.module, req, res);
});

app.get('/', function(req, res){
  res.sendFile(path.join(__dirname + '/index.html'));
});

app.post('/hook/*', function(req, res) {
  res.sendStatus(200);
});

var httpPort = process.env.PORT || 3000;
app.listen(httpPort);

