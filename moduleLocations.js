module.exports = {
    adminPage: [
        "org.bitbucket.repository.admin",
        "org.bitbucket.account.admin"
    ],
    repoPage: [
        "org.bitbucket.repository.actions",
        "org.bitbucket.repository.navigation"
    ],
    webItem: [
        "org.bitbucket.repository.actions",
        "org.bitbucket.repository.navigation",
        "org.bitbucket.pullrequest.summary.actions",
        "org.bitbucket.pullrequest.summary.info",
        "org.bitbucket.branch.summary.actions",
        "org.bitbucket.branch.summary.info",
        "org.bitbucket.commit.summary.actions",
        "org.bitbucket.commit.summary.info"
    ],
    webPanel: [
        "org.bitbucket.repository.overview.informationPanel"
    ]
};
