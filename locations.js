module.exports = [
    "org.bitbucket.repository.admin",
    "org.bitbucket.account.admin",
    "org.bitbucket.repository.navigation",
    "org.bitbucket.repository.actions",
    "org.bitbucket.pullrequest.summary.actions",
    "org.bitbucket.pullrequest.summary.info",
    "org.bitbucket.branch.summary.actions",
    "org.bitbucket.branch.summary.info",
    "org.bitbucket.commit.summary.actions",
    "org.bitbucket.commit.summary.info",
    "org.bitbucket.repository.overview.informationPanel"
];