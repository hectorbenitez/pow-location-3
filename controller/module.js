var contextParameters = require("../contextParameters");
var locations = require("../locations");
var _ = require("lodash");
var exampleDescriptor = require('./descriptor').template;

exports.view = function(module, req, res) {
  var params = _.pick(req.query, function (val, key) {
    return val && ["e", "l", "jwt", "xdm_e", "xdm_c", "ui-params"].indexOf(key) === -1;
  });
  params = _.mapKeys(params, function(value, key) {
    return contextParameters[key] || key;
  });
  params = _.pick(params, function(value, key) {
    // don't display unsubstituted values that have been passed through
    return value !== '{' + key + '}';
  });
  var location = locations[req.query.l];
  var template = req.query.e !== undefined ? 'moduleEmbedded' : 'modulePage';

  // build some example JSON
  var exampleJson = null;
  var exampleModule = _.find(exampleDescriptor.modules[module], function (module) {
      return module.location === location;
  });
  if (exampleModule) {
    exampleModule = _.cloneDeep(exampleModule);
    exampleModule.url = "/somewhere_better";
    exampleModule.key = "key-" + Math.floor(Math.random() * 65536).toString(16);
    exampleJson = {
        key: "your-amazing-addon",
        name: "Your amazing addon",
        baseUrl: "https://amazing.example.com/",
        modules:{}
    };
    exampleJson.modules[module] = [exampleModule];
    exampleJson = JSON.stringify(exampleJson, null, 4);
  }

  res.render(template, {
    title: module + " - " + location,
    module: module,
    location: location,
    contextParameters: params,
    exampleJson: exampleJson
  });
};
