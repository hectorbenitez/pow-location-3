var contextParameters = require("../contextParameters");
var locations = require("../locations");
var moduleLocations = require("../moduleLocations");

var template = {
  "baseUrl": process.env.PL3_BASE_URL,
  "key": "pow-location-3",
  "authentication": {
    "type": "jwt"
  },
  "vendor": {
    "name": "Atlassian",
    "url": "https://www.atlassian.com/"
  },
  "description": "An example add-on highlighting Bitbucket's module types and locations.",
  "name": "Location, Location, Location",
  "modules": {

    "configurePage": [],
    "adminPage": [],
    "repoPage": [],
    "profileTab": [],
    "webPanel": [],
    "webItem": [],
    "oauthConsumer": {
      "clientId": process.env.BB_OAUTH_CONSUMER
    },
    "webhooks": [{
      event: "*",
      url: "/hook/"
    }]
  },
  "scopes": ["account"],
  "contexts": ["personal"]
};

function appendContextParameters(url) {
  contextParameters.forEach(function (param, contextParameterIndex) {
    url += "&" + contextParameterIndex + "={" + param + "}";
  });
  return url;
}

// location-less modules
["configurePage", "profileTab"].forEach(function(module) {
  var url = appendContextParameters("/m/" + module + "?e=1"); // render embedded without chrome

  // HACK: configurePage & profileTab URIs are currently limited to 255 characters (BB-14481)
  url = url.substring(0, url.lastIndexOf("&", 255));

  template.modules[module].push({
    url: url,
    name: {
      value: module + " module"
    },
    key: module
  });
});

var md5 = require("MD5");

["adminPage", "repoPage", "webItem", "webPanel"].forEach(function(module) {
  moduleLocations[module].forEach(function(location) {

    var url = "/m/" + module + "?l=" + locations.indexOf(location);
    url = appendContextParameters(url);
    if (module !== "webItem") url += "&e=1"; // render embedded without chrome

    template.modules[module].push({
      url: url,
      name: {
        value: location
      },
      location: location,
      key: module + "-" + md5(location),
      "params": {
        "auiIcon": "aui-iconfont-homepage"
      }
    });
  });
});

exports.descriptor = function(req, res) {
  res.send(template);
};

exports.template = template;
