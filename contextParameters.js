module.exports = [
    "user_username",
    "user_uuid",
    "user_type",
    "repo_path",
    "repo_uuid",
    "repo_owner_uuid",
    "repo_https_url",
    "repo_ssh_url",
    "repo_language",
    "file_cset",
    "file_path",
    "file_name",
    "branch_name",
    "branch_resolved_hash",
    "commit_hash",
    "pullrequest_id",
    "pullrequest_anchor",
    "pullrequest_source_branch"
];
