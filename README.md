# Location, Location, Location

An example add-on highlighting Bitbucket's module types and locations.

# Installation

Use the **Install Location, Location, Location** button at [pow-location-3.herokuapp.com](http://pow-location-3.herokuapp.com/)

# Installation (local)

1. Install [Git](http://git-scm.com/), [Node.js](http://nodejs.org/) (tested against v0.10.36),
[npm](https://npmjs.org/) and [ngrok](https://ngrok.com/).
1. Clone this repository.
1. Run `npm install`.
1. Run `ngrok 3000`.
1. From the ngrok output copy the *https* **Forwarding URL** (e.g. `https://1584ce3.ngrok.com`).
1. Set the URL you copied as the value of the environment variable `PL3_BASE_URL`.
1. Go to https://bitbucket.org/account/user/yourusername/api and `Add consumer`. Paste in the previous url into the URL field.
1. Set the OAuth key you just created as the value of the environment varialbe `BB_OAUTH_CONSUMER`.
1. Run `node app.js`.
1. Go to `bitbucket.org` and then browse to **Bitbucket Settings** > **Manage add-ons** > **Install add-on from URL**
1. Paste the URL you copied into the **Add-on URL** field and click **Add**.